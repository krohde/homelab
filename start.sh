#!/bin/bash
set -Eeuo pipefail


echo "Installing Ansible on ansible controller .."

sudo apt update
sudo apt -y install ansible
ansible --version

[[ -e /etc/ansible/hosts ]] && sudo mv /etc/ansible/hosts /etc/ansible/hosts.$(date +%Y%m%d)

sudo sh -c "echo '# Ansible hosts
[all_docker]
homelab ansible_connection=local
' > /etc/ansible/hosts"

ansible -m ping all

[[ -e requirements.yml ]] && ansible-galaxy install -r requirements.yml
[[ -e requirements.yml ]] && ansible-galaxy collection install -r requirements.yml

echo "Examples:"
echo "ansible-playbook playbooks/01.XYZ.yml --ask-become-pass"
echo "ansible-playbook playbooks/02.docker_container_portainer.yml -e 'ansible_python_interpreter=python3'"
